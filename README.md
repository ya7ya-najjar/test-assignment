# Test Assignment  
## Laravel 8

### Introduction

...

### Installation (first time)

1. Open in cmd or terminal app and navigate to this folder
2. Run following commands

```bash
composer install
```

```bash
cp .env.example .env
```

 - open .env in any text editor and change APP_URL to the assigned domain 
######    .  .  .  for example : https://test.com
 - set the database information in .env
######    .  .  .  for example : DB_DATABASE : quiz_website
 
```bash
php artisan key:generate
```

```bash
npm install
```

```bash
npm run dev
```

```bash
php artisan migrate --seed
```

```bash
php artisan storage:link
```


if you run in locale machine:
```bash
php artisan serve
```
...

### Ya7ya Alnajjar



