<?php

namespace Database\Seeders;

use App\Models\Quiz;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class ExtraQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // First create main Quiz
        $quiz = Quiz::create([
           'slug' => Uuid::uuid4()
        ]);

        // Create Questions and associate them to the previous quiz
        $question = $quiz->questions()->create([
            'ar' => ['sentence' => 'ما ناتج ضرب 8 مع 7'],
            'en' => ['sentence' => 'What is the result 8 times 7?']
        ]);

        // Create Answers and associate them to the previous question
        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 7'],
            'en' => ['sentence' => 'The result is 7'],
            'correct' => 0,
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 18'],
            'en' => ['sentence' => 'The result is 18'],
            'correct' => 0,
            'sort_order' => 1
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 50'],
            'en' => ['sentence' => 'The result is 50'],
            'correct' => 1,
            'sort_order' => 2
        ]);


        ////////////////////////////////////////////////////////
        // Create new Questions and associate them to the previous quiz
        $question = $quiz->questions()->create([
            'ar' => ['sentence' => 'ما ناتج جمع 8 مع 7'],
            'en' => ['sentence' => 'What is the result 8 plus 7?']
        ]);

        // Create Answers and associate them to the previous question
        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 7'],
            'en' => ['sentence' => 'The result is 7'],
            'correct' => 0,
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 18'],
            'en' => ['sentence' => 'The result is 18'],
            'correct' => 0,
            'sort_order' => 1
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 15'],
            'en' => ['sentence' => 'The result is 15'],
            'correct' => 1,
            'sort_order' => 2
        ]);

        ////////////////////////////////////////////////////////
        // Create new Questions and associate them to the previous quiz
        $question = $quiz->questions()->create([
            'ar' => ['sentence' => 'ما ناتج طرح 8 مع 7'],
            'en' => ['sentence' => 'What is the result 8 minus 7?']
        ]);

        // Create Answers and associate them to the previous question
        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 7'],
            'en' => ['sentence' => 'The result is 7'],
            'correct' => 0,
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 18'],
            'en' => ['sentence' => 'The result is 18'],
            'correct' => 0,
            'sort_order' => 1
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 5'],
            'en' => ['sentence' => 'The result is 5'],
            'correct' => 1,
            'sort_order' => 2
        ]);

    }
}
