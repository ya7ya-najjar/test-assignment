<?php

namespace Database\Seeders;

use App\Models\Quiz;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // First create main Quiz
        $quiz = Quiz::create([
           'slug' => Uuid::uuid4()
        ]);

        // Create Questions and associate them to the previous quiz
        $question = $quiz->questions()->create([
            'ar' => ['sentence' => 'ما ناتج ضرب 5 مع 10'],
            'en' => ['sentence' => 'What is the result 10 times 5?']
        ]);

        // Create Answers and associate them to the previous question
        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 7'],
            'en' => ['sentence' => 'The result is 7'],
            'correct' => 0,
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 18'],
            'en' => ['sentence' => 'The result is 18'],
            'correct' => 0,
            'sort_order' => 1
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 50'],
            'en' => ['sentence' => 'The result is 50'],
            'correct' => 1,
            'sort_order' => 2
        ]);


        ////////////////////////////////////////////////////////
        // Create new Questions and associate them to the previous quiz
        $question = $quiz->questions()->create([
            'ar' => ['sentence' => 'ما ناتج جمع 5 مع 10'],
            'en' => ['sentence' => 'What is the result 10 plus 5?']
        ]);

        // Create Answers and associate them to the previous question
        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 7'],
            'en' => ['sentence' => 'The result is 7'],
            'correct' => 0,
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 18'],
            'en' => ['sentence' => 'The result is 18'],
            'correct' => 0,
            'sort_order' => 1
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 15'],
            'en' => ['sentence' => 'The result is 15'],
            'correct' => 1,
            'sort_order' => 2
        ]);

        ////////////////////////////////////////////////////////
        // Create new Questions and associate them to the previous quiz
        $question = $quiz->questions()->create([
            'ar' => ['sentence' => 'ما ناتج طرح 5 مع 10'],
            'en' => ['sentence' => 'What is the result 10 minus 5?']
        ]);

        // Create Answers and associate them to the previous question
        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 7'],
            'en' => ['sentence' => 'The result is 7'],
            'correct' => 0,
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 18'],
            'en' => ['sentence' => 'The result is 18'],
            'correct' => 0,
            'sort_order' => 1
        ]);

        $question->answers()->create([
            'ar' => ['sentence' => 'الناتج 5'],
            'en' => ['sentence' => 'The result is 5'],
            'correct' => 1,
            'sort_order' => 2
        ]);

    }
}
