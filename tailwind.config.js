module.exports = {
    purge: [],
    theme: {
        extend: {
            borderRadius: {
                semiFull: '3rem'
            },
            colors: {
                cyanBlue: '#344255',
                offWhite: '#f6f6f6',
                purpleSpecial: '#835bec',
                bgMain: '#202f43',
                blueCustom: '#06b3fc',
                purpleLight: '#784bec',
                yellowLight: '#fec313'
            },
            spacing: {
                '7':'1.75rem',
                '9': '2.25rem',
                '11': '2.75rem',
                '13': '3.25rem',
                '40': '12rem',
                '70': '20rem',
                '80': '30rem'
            }
        },
    },
    variants: {},
    plugins: [
        require('tailwindcss-rtl')
    ],
};
