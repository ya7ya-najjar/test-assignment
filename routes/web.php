<?php

use App\Http\Controllers\HomePageController;
use App\Http\Controllers\QuizController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomePageController::class, 'index'])->name('homepage');
Route::get('/quiz', [HomePageController::class, 'quiz'])->name('quiz');
Route::get('/results', [HomePageController::class, 'results'])->name('results');

Route::post('/quiz', [QuizController::class, 'save'])->name('save-quiz');
