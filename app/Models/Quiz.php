<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use HasFactory;

    protected $fillable = ['slug'];

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function finalResults()
    {
        return $this->hasMany(FinalResult::class);
    }
}
