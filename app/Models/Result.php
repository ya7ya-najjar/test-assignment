<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;

    protected $fillable = ['correct', 'question_id', 'answer_id', 'quiz_id'];

    public $timestamps = false;
}
