<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model implements TranslatableContract
{
    use HasFactory;
    use Translatable;

    protected $fillable = [ 'image', 'correct', 'sort_order'];
    public $translatedAttributes = ['sentence'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

}
