<?php

namespace App\Repositories;

use App\Models\Answer;
use App\Models\FinalResult;
use App\Models\Question;
use App\Models\Result;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class QuizRepository{


    public function add(Request $request)
    {
        $data = $this->calculateDataForQuiz($request);
        $result = $this->calculateResultForQuiz(collect($data));
        try {
            DB::beginTransaction();
            Result::insert($data);
            FinalResult::create(['total_points' => $result, 'quiz_id' => $request->get('quiz_id')]);

        }catch (Exception $exception){
            DB::rollBack();
            Log::error($exception->getMessage());

            $request->session()->flash('error', 'Something went wrong');
            return view('index');
        }

        DB::commit();

        return redirect(action('HomePageController@results'));

    }

    public function getQuizQuestions($quiz = null)
    {
        if ($quiz == null)
            return Question::orderBy('sort_order')->get();

        return $quiz->questions()->orderBy('sort_order')->get();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function calculateDataForQuiz(Request $request)
    {
        $questionsIds = getQuestionsIds($request);
        $questionsAnswers = Answer::with('translations')->whereIn('question_id', $questionsIds)->get();
        $data = [];
        foreach ($questionsAnswers as $questionsAnswer) {
            $userAnswerId = intval($request->get('question-' . $questionsAnswer->question_id));
            $userAnswer = Answer::find($userAnswerId);
            $correctAnswer = $questionsAnswer->correct && $userAnswer->correct;

            if ($userAnswerId === $questionsAnswer->id)
                $data[] = [
                    'quiz_id' => $request->get('quiz_id'),
                    'question_id' => $questionsAnswer->question_id,
                    'answer_id' => $questionsAnswer->id,
                    'correct' => $correctAnswer
                ];
        }
        return $data;
    }

    public function calculateResultForQuiz(Collection $data)
    {
        $points = (int) $data->where('correct', true)->count();
        return  $points <= 0 ? 1 : $points;
    }
}
