<?php

namespace App\Http\Controllers;

use App\Models\FinalResult;
use App\Models\Quiz;
use App\Repositories\QuizRepository;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class HomePageController extends Controller
{
    private $quizRepository;

    /**
     * Create a new controller instance.
     *
     * @param QuizRepository $quizRepository
     */
    public function __construct(QuizRepository $quizRepository)
    {
        $this->quizRepository = $quizRepository;
    }

    public function index()
    {
        return view('index');
    }

    public function quiz(Request $request)
    {
        $quiz = Quiz::whereDoesntHave('finalResults')->first();
        if ($quiz == null){
            $request->session()->flash('error', 'There is no quizzes left.');
            return view('index');
        }
        $questions = $this->quizRepository->getQuizQuestions($quiz);
        return view('quiz', compact('questions', 'quiz'));
    }

    public function results()
    {
        $results = FinalResult::all();
        return view('results', compact('results'));
    }

}
