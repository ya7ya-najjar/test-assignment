<?php

namespace App\Http\Controllers;

use App\Repositories\QuizRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    private $quizRepository;

    public function __construct(QuizRepository $quizRepository)
    {
        $this->quizRepository = $quizRepository;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request)
    {
        $this->quizRepository->add($request);
        $request->session()->flash('success', 'article created successfully');

        return redirect()->route('results');
    }
}
