<?php

use Illuminate\Http\Request;

function getQuestionsIds(Request $request)
{
    $questions = array_filter(array_keys($request->all()), function ($key) {
        return strpos($key, 'question-') === 0;
    });
    return array_map(function ($value) {
        return (int)str_replace('question-', '', $value);
    }, array_values($questions));
}
