<x-master title="Results">
    @section('breadcrumb')/<a href="" class="text-gray-300 px-2">  results</a>@endsection
        <div class="mt-3 p-6 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
            @foreach($results as $result)
                <div class="mb-4 bg-blueCustom flex justify-between rounded-lg items-center pb-4 overflow-hidden
                 transition duration-500 ease-in-out transform hover:-translate-y-2 hover:shadow-lg">
                    <p class="text-white text-xl font-medium px-12 py-8">
                       Total Points : {{ $result->total_points  }}
                        <br>
                        Quiz ID : {{ $result->quiz_id }}
                    </p>
                </div>
            @endforeach
        </div>
</x-master>
