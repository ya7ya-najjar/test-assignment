<x-master title="Quiz">
        @section('breadcrumb')/<a href="" class="text-gray-300 px-2">  quiz</a>@endsection

            <form action="{{ route('save-quiz') }}" method="POST">
                @csrf
                <input type="hidden" name="quiz_id" value="{{ $quiz->id }}">
                @foreach($questions as $question)
                   <div class="mt-3 p-6 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <span class="text-gray-700 text-xl font-medium">
                        <span class="font-bold text-xl mx-2" style="color: #784bec">{{ $loop->index + 1 }}.</span>
                        {{ $question->sentence }}
                    </span>

                       <div class="mt-4 flex justify-center items-center">
                           @foreach($question->answers as $answer)
                               <div class="text-center mx-2 md:mx-6">
                                   <label class="cursor-pointer inline-flex items-center py-4 mt-2">
                                       <input type="radio"
                                              class="cursor-pointer form-radio h-6 w-6 border-2 border-gray-600 mr-2"
                                              name="question-{{ $question->id }}"
                                              value="{{ $answer->id }}">
                                       <span class="hover:text-blue-700 mr-2 text-gray-700 text-lg">{{ $answer->sentence }}</span>
                                   </label>
                               </div>
                           @endforeach
                       </div>
                   </div>
                @endforeach
                <div class="flex justify-center">
                    <button
                        type="submit"
                        class="text-white bg-blueCustom border-2 border-opacity-0 text-2xl mt-8 py-2 px-6 rounded-full transition duration-150 hover:bg-white hover:text-blueCustom hover:border-blueCustom">
                        Finish Quiz
                    </button>
                </div>
            </form>
</x-master>
