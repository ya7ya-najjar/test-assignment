<!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('layouts._meta')
        @yield('style')
    </head>
    <body class="bg-bgMain">
        @include('layouts._header')
        <div class="rounded-semiFull bg-white relative flex items-top min-h-screen sm:items-center sm:pt-0">
            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
                {{ $slot }}
            </div>
        </div>

        @yield('scripts')
    </body>
</html>
