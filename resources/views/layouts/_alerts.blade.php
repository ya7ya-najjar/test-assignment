<div id="alert">
    @if($errors->any())
        <div class="w-3/4 mx-auto flex flex-col p-5 m-5 rounded-lg bg-red-300 text-red-900">
            @foreach($errors->all() as $error)
                <p>- {{ $error }}</p>
            @endforeach
        </div>
    @endif

        @if($message = \Session::get('error'))
            <div class="w-3/4 mx-auto flex flex-col p-5 m-5 rounded-lg bg-red-300 text-red-900">
                <p>{{ $message }}</p>
            </div>
        @endif

    @if($message = \Session::get('success'))
        <div class="w-3/4 mx-auto flex flex-col p-5 m-5 rounded-lg bg-green-300 text-green-900">
            <p>{{ $message }}</p>
        </div>
    @endif

</div>

@if($message = Session::get('status'))
    <div class="w-3/4 mx-auto flex flex-col p-5 m-5 rounded-lg bg-green-300 text-green-900">
        <p>{{ $message }}</p>
    </div>
@endif
