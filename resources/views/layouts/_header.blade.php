<header class="relative bg-no-repeat bg-cover bg-center pb-6"
        style="background-image: url({{ asset('header-dashboard.png') }});">
    <div class="container mx-auto">
        <h2 class="text-white text-center text-3xl md:text-5xl font-bold pt-8">
            {{ $title }}
        </h2>
        <div class="breadcrumb text-white text-xs md:text-lg flex justify-center pt-4">
            <a href="{{ route('homepage') }}" class="hover:text-purple-200 px-2">Homepage</a>
            @yield('breadcrumb')
        </div>
    </div>
</header>
